#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

export AWS_DEFAULT_REGION=${VPCREGION:-eu-west-2}

# Assume role and exports AWS credentials
aws_assumerole() {
  local aws_account_number=$1
  local role_name=$2
  temp_role=$(aws sts assume-role \
                    --role-arn "arn:aws:iam::$aws_account_number:role/$role_name" --role-session-name "$ROLESESSNAME")
  AWS_ACCESS_KEY_ID=$(echo "$temp_role" | jq -r .Credentials.AccessKeyId)
  export AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY=$(echo "$temp_role" | jq -r .Credentials.SecretAccessKey)
  export AWS_SECRET_ACCESS_KEY
  AWS_SESSION_TOKEN=$(echo "$temp_role" | jq -r .Credentials.SessionToken)
  export AWS_SESSION_TOKEN
}

# unset the AWS credetials from environment variables
unset_aws_credentails_exports() {
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
}

# Route53 create VPC association Authorization
aws_assumerole "$LOCACCID" "$ROLE"
aws route53 create-vpc-association-authorization --hosted-zone-id "$PHZ" --vpc VPCRegion="$AWS_DEFAULT_REGION",VPCId="$DNSVPC"
unset_aws_credentails_exports

# Route53 Association VPC with hosted zone
aws_assumerole "$NETACCID" "$ROLE"
aws route53 associate-vpc-with-hosted-zone --hosted-zone-id "$PHZ" --vpc VPCRegion="$AWS_DEFAULT_REGION",VPCId="$DNSVPC"
unset_aws_credentails_exports

# Delete the VPC Association Authorization
aws_assumerole "$LOCACCID" "$ROLE"
aws route53  delete-vpc-association-authorization --hosted-zone-id "$PHZ" --vpc VPCRegion="$AWS_DEFAULT_REGION",VPCId="$DNSVPC"
unset_aws_credentails_exports