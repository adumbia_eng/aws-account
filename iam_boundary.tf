# Baseline Boundary Policy, merged with account specific Baseline below
data "aws_iam_policy_document" "iam_boundary_baseline" {
  count = var.ci_baseline == "" ? 0 : 1
  source_json = templatefile(
    var.ci_boundary_baseline, {
      account_id       = aws_organizations_account.account.id,
      kms_decrypt_cmks = jsonencode(var.kms_cmk_decrypt)
      kms_use_cmks     = jsonencode(var.kms_cmk_use)
    }
  )
}

# Boundary to be used by all CIDeploy Roles
data "aws_iam_policy_document" "iam_boundary" {
  count = var.ci_baseline == "" ? 0 : 1
  source_json = templatefile(
    var.ci_boundary,
    merge(
      var.iam_boundary_variables,
      {
        account_id = aws_organizations_account.account.id,
        region     = var.region,
      }
    )
  )
}

# Combine `iam_boundary_baseline` and `iam_boundary` together
data "aws_iam_policy_document" "iam_boundary_final" {
  count         = var.ci_baseline == "" ? 0 : 1
  source_json   = data.aws_iam_policy_document.iam_boundary_baseline[0].json
  override_json = data.aws_iam_policy_document.iam_boundary[0].json
}

# Finally create the policy
resource "aws_iam_policy" "iam_boundary" {
  count       = var.ci_baseline == "" ? 0 : 1
  provider    = aws.account
  name        = "GELBoundary"
  description = "Boundary policy for IAM Roles"
  policy      = data.aws_iam_policy_document.iam_boundary_final[count.index].json
}
