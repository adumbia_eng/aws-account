locals {
  dev_ou_id = "ou-vg3y-j20jg9v6"
  example-account_apps = {
    Foo = {
      policy = "${path.module}/policies/CIAppRole_example-account_Foo.json"
    },
    Bar = {
      policy = "${path.module}/policies/CIAppRole_example-account_Bar.json"
    }
  }
  region                 = "eu-west-2"
  core_shared_account_id = "0123456789"
  iam_boundary_variables = {
    "instance_id" = "i-1234567890"
  }
  kms_key = "arn:aws:kms:eu-west-2:123456789012:key/47f517db-aefe-4388-8187-0d5b360902ed"
}

module "account_example-account_dev" {
  source = "../"

  name        = "example-account_dev"
  email_owner = "example-account_dev@example.com"

  organizational_unit_id = local.dev_ou_id
  role_name              = "OrgDeploy"
  region                 = local.region

  environment = "test" # prod or tetst
  ad_groups = {
    APP-AWS-SSO-Access-Example-Account = ["AdministratorAccess", "Billing"] # <AD Group> = [<PermissionSet>, ...]
  }

  budget_limit_amount = "1000.0"
  budget_emails = [
    "user1@example.com",
    "user2@example.com",
  ]

  terraform_state_manager_role_arns = ["arn:aws:iam::123456789012:role/OrgDeploy"]
  terraform_state_reader_role_arns  = ["arn:aws:iam::012345678901:role/CIDeploy"]

  applications         = local.example-account_apps
  shared_account       = local.core_shared_account_id
  ci_baseline          = "${path.module}/policies/Baseline.json"
  ci_boundary          = "${path.module}/policies/Boundary.json"
  ci_boundary_baseline = "${path.module}/policies/BoundaryBaseline.json"

  iam_boundary_variables = local.iam_boundary_variables

  aws_config_enabled     = true
  aws_config_role_name   = "GELConfig"
  aws_config_bucket_name = "example-aws-config-bucket"

  aws_config_rules_aws = {
    restricted-ssh = {}
    approved-amis-by-tag = {
      allowed_tags = "foo:bar"
    }
  }

  kms_cmk_decrypt     = [local.kms_key]
  kms_cmk_use         = [local.kms_key]
  kms_cmk_ebs_default = local.kms_key

  logging_account = "012345678901"

  cloudhealth_partner_external_id  = "abc123"
  cloudhealth_customer_external_id = "123abc"

  tags = {
    dept = "cloud-platform",
    func = "AWS organization",
  }

  ci_tags = {
    "Department" : "Example Department",
    "Squad" : "Example Squad"
  }

  create_private_hosted_zones = true
  vpc_name                    = "test"
  network_account             = "112596477968"
  route53_resolver_rules      = ["rslvr-rr-123456789"]
  private_hosted_zones        = ["example-account.example.com"]

  iam_credentials_report_recipients = ["address@example.com"]

  ses_iam_role_arn     = "arn:aws:iam::123456789000:role/SESSendEmail"
  ses_email_source     = "address@example.com"
  ses_email_source_arn = "arn:aws:ses:eu-west-2:123456789000:identity/address@example.com"
}
