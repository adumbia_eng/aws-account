locals {
  config_tags = {
    "Service" : "AWS Config"
  }
}

## AWS Config resources
# Setup the config recorder to assume the role above
resource "aws_config_configuration_recorder" "platform" {
  provider = aws.account
  count    = var.aws_config_enabled ? 1 : 0

  name     = "default"
  role_arn = "arn:aws:iam::${aws_organizations_account.account.id}:role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig"

  recording_group {
    include_global_resource_types = true
  }

  depends_on = [null_resource.account_delay]
}

# Setup config to use the bucket specified
resource "aws_config_delivery_channel" "platform" {
  provider = aws.account
  count    = var.aws_config_enabled ? 1 : 0

  s3_bucket_name = var.aws_config_bucket_name

  snapshot_delivery_properties {
    delivery_frequency = "TwentyFour_Hours"
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]
}

# Enable the config recorder, this needs the config recorder setup and the
# delivery channel setup before enabling.
resource "aws_config_configuration_recorder_status" "platform" {
  provider = aws.account
  count    = var.aws_config_enabled ? 1 : 0

  name       = aws_config_configuration_recorder.platform[count.index].name
  is_enabled = true

  depends_on = [
    aws_config_delivery_channel.platform
  ]
}

## Config Accregator
resource "aws_config_aggregate_authorization" "aggregation_authorization" {
  provider = aws.account
  count    = var.aws_config_enabled ? 1 : 0

  account_id = var.logging_account
  region     = var.region

  tags = merge(var.tags, merge(local.config_tags, {
    "Component" : "Config Aggregator Autorization"
  }))

  depends_on = [null_resource.account_delay]
}
