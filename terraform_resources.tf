locals {
  tf_state_bucket_name   = "${aws_organizations_account.account.id}-terraform"
  account_org-deploy_arn = "arn:aws:iam::${aws_organizations_account.account.id}:role/OrgDeploy"
}

# Create S3 Bucket for Terrafom
resource "aws_s3_bucket" "terraform" {
  provider = aws.account

  bucket = local.tf_state_bucket_name
  policy = data.aws_iam_policy_document.terraform.json

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    noncurrent_version_expiration {
      days = 90
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = var.tags

  depends_on = [null_resource.account_delay]
}

resource "aws_s3_bucket_public_access_block" "terraform" {
  provider = aws.account

  bucket = aws_s3_bucket.terraform.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Terraform state bucket policy
data "aws_iam_policy_document" "terraform" {
  statement {
    sid = "AllowReadObjects"

    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:ListBucket"
    ]

    resources = [
      "arn:aws:s3:::${local.tf_state_bucket_name}",
      "arn:aws:s3:::${local.tf_state_bucket_name}/*",
    ]

    principals {
      type = "AWS"
      identifiers = compact(
        concat(
          [
            for role in aws_iam_role.ci_deploy :
            role.arn
          ],
          [
            local.account_org-deploy_arn
          ],
          var.terraform_state_reader_role_arns,
          var.terraform_state_manager_role_arns,
        )
      )
    }
  }

  statement {
    sid = "AllowWriteObjects"

    effect = "Allow"

    actions = [
      "s3:PutObject"
    ]

    resources = [
      "arn:aws:s3:::${local.tf_state_bucket_name}/*",
    ]

    principals {
      type = "AWS"
      identifiers = compact(
        concat(
          [
            for role in aws_iam_role.ci_deploy :
            role.arn
          ],
          [
            local.account_org-deploy_arn
          ],
          var.terraform_state_manager_role_arns
        )
      )
    }
  }

  statement {
    sid = "PrevenHttpRequests"

    effect = "Deny"

    actions = [
      "s3:*"
    ]

    resources = [
      "arn:aws:s3:::${local.tf_state_bucket_name}",
      "arn:aws:s3:::${local.tf_state_bucket_name}/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values = [
        "false"
      ]
    }
  }

  statement {
    sid = "DenyIncorrectEncryptionHeader"

    effect = "Deny"

    actions = [
      "s3:PutObject"
    ]

    resources = [
      "arn:aws:s3:::${local.tf_state_bucket_name}/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values = [
        "AES256"
      ]
    }
  }

  statement {
    sid = "DenyUnencryptedObjectUploads"

    effect = "Deny"

    actions = [
      "s3:PutObject"
    ]

    resources = [
      "arn:aws:s3:::${local.tf_state_bucket_name}/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = [
        true
      ]
    }
  }
}

# Create the DynamoDB Table for Terraform
resource "aws_dynamodb_table" "terraform" {
  provider = aws.account

  name         = "terraform-state-lock_${var.name}"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = var.tags

  depends_on = [null_resource.account_delay]
}
