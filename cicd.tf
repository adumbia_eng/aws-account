## Setup the roles needed for CICD from GitLab Runner Workers
# Policy / Role for the GitLab worker to assume
data "aws_iam_policy_document" "ci_assumerole" {
  statement {
    sid     = "CIAssumeRole"
    actions = ["sts:AssumeRole"]
    principals {
      type = "AWS"
      identifiers = [
        var.shared_account,
        aws_organizations_account.account.id,
      ]
    }
  }
}

resource "aws_iam_role" "ci_deploy" {
  provider = aws.account
  for_each = var.applications

  name                 = "CIDeploy${each.key}"
  assume_role_policy   = data.aws_iam_policy_document.ci_assumerole.json
  permissions_boundary = aws_iam_policy.iam_boundary[0].arn

  tags = merge(var.ci_tags, {
    "Service" : "CI",
    "Component" : "GitLab Worker",
    "DataClassification" : "Private",
  })
}

# Default CIDeploy Policy, merged with app specific role below
data "aws_iam_policy_document" "ci_deploy_baseline" {
  count = var.ci_baseline == "" ? 0 : 1
  source_json = templatefile(
    var.ci_baseline, {
      account_name     = var.name
      account_id       = aws_organizations_account.account.id,
      region           = var.region,
      kms_decrypt_cmks = jsonencode(var.kms_cmk_decrypt)
      kms_use_cmks     = jsonencode(var.kms_cmk_use)
  })
}

# App specific role
data "aws_iam_policy_document" "ci_deploy_app" {
  for_each = var.applications
  source_json = templatefile(
    "${each.value.policy}", {
      account_name = var.name
      account_id   = aws_organizations_account.account.id,
      region       = var.region,
  })
}

# Combine `ci_deploy_baseline` and `ci_deploy_app` together
data "aws_iam_policy_document" "ci_deploy_final" {
  for_each      = var.applications
  source_json   = data.aws_iam_policy_document.ci_deploy_baseline[0].json
  override_json = data.aws_iam_policy_document.ci_deploy_app[each.key].json
}

# Apply the combined policy as the inline policy for the CIDeploy role
resource "aws_iam_role_policy" "ci_deploy" {
  provider = aws.account
  for_each = var.applications

  name = "CIDeploy${each.key}"
  role = aws_iam_role.ci_deploy[each.key].id

  policy = data.aws_iam_policy_document.ci_deploy_final[each.key].json
}
