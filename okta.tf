# Okta API Token
data "aws_secretsmanager_secret_version" "okta_token" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/okta/api_token-PGwr94"
}

# Create the Okta group
resource "okta_group" "group" {
  for_each = var.ad_groups

  name = substr(replace(replace(each.key, "Access", "${title(var.environment)}-Org"), "-", "_"), 4, 999)
}

# Create a rule to auto-populate the Okta group from the AD group
resource "okta_group_rule" "rule" {
  for_each = var.ad_groups

  name              = substr(replace(replace(each.key, "Access", "${title(var.environment)}-Org"), "-", " "), 4, 999)
  status            = "ACTIVE"
  group_assignments = [okta_group.group[each.key].id]
  expression_type   = "urn:okta:expression:1.0"
  expression_value  = "isMemberOfGroupName(\"${each.key}\")"
}

# Find the AWS SSO application in Okta
data "okta_app" "aws_sso" {
  label       = "AWS SSO in ${title(var.environment)} Org"
  active_only = true
}

# Assign the Okta group to the Okta AWS application 
resource "okta_app_group_assignment" "aws_sso" {
  for_each = var.ad_groups

  app_id   = data.okta_app.aws_sso.id
  group_id = okta_group.group[each.key].id
}

# Write the mapping data to DynamoDB
data "aws_dynamodb_table" "aws_sso_mapping" {
  name = "SSO-Group-Mapping"
}

resource "aws_dynamodb_table_item" "aws_sso_mapping" {
  for_each = var.ad_groups

  table_name = data.aws_dynamodb_table.aws_sso_mapping.name
  hash_key   = data.aws_dynamodb_table.aws_sso_mapping.hash_key
  item = jsonencode({
    pk             = { S = "${aws_organizations_account.account.id}#${okta_group.group[each.key].name}" }
    Account        = { S = aws_organizations_account.account.id }
    Group          = { S = okta_group.group[each.key].name }
    PermissionSets = { S = join(",", each.value) }
  })
}