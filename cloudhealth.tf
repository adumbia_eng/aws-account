resource "aws_iam_role" "cloudhealth" {
  provider = aws.account
  count    = var.allow_cloudhealth_integrations ? 1 : 0

  name        = "CloudHealth"
  description = "IAM Role which can be assumed by CloudHealth (third-party software)"
  path        = "/"

  assume_role_policy    = concat(data.aws_iam_policy_document.cloudhealth_trust.*.json, [""])[0]
  force_detach_policies = true

  max_session_duration = 3600
}

resource "aws_iam_policy" "cloudhealth_read_only" {
  provider = aws.account
  count    = var.allow_cloudhealth_integrations ? 1 : 0

  path        = "/"
  name        = "CloudHealthReadOnly"
  description = "Read only access to resources for the purposes of billing management"
  policy      = concat(data.aws_iam_policy_document.cloudhealth_read_only.*.json, [""])[0]
}

resource "aws_iam_role_policy_attachment" "cloudhealth_read_only" {
  provider = aws.account
  count    = var.allow_cloudhealth_integrations ? 1 : 0

  role       = aws_iam_role.cloudhealth[count.index].name
  policy_arn = aws_iam_policy.cloudhealth_read_only[count.index].arn
}


data "aws_iam_policy_document" "cloudhealth_trust" {
  count = var.allow_cloudhealth_integrations ? 1 : 0

  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::454464851268:root"]
    }

    actions = ["sts:AssumeRole"]

    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"

      values = [
        var.cloudhealth_customer_external_id
      ]
    }
  }

  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::454464851268:root"]
    }

    actions = ["sts:AssumeRole"]

    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"

      values = [
        var.cloudhealth_partner_external_id
      ]
    }
  }
}

data "aws_iam_policy_document" "cloudhealth_read_only" {
  count = var.allow_cloudhealth_integrations ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "autoscaling:Describe*",
      "aws-portal:ViewBilling",
      "aws-portal:ViewUsage",
      "cloudformation:List*",
      "cloudformation:DescribeStack*",
      "cloudformation:GetTemplate",
      "cloudfront:Get*",
      "cloudfront:List*",
      "cloudtrail:DescribeTrails",
      "cloudtrail:GetEventSelectors",
      "cloudtrail:ListTags",
      "cloudwatch:Describe*",
      "cloudwatch:Get*",
      "cloudwatch:List*",
      "config:Get*",
      "config:Describe*",
      "config:Deliver*",
      "config:List*",
      "cur:Describe*",
      "dms:Describe*",
      "dms:List*",
      "dynamodb:DescribeTable",
      "dynamodb:List*",
      "ec2:Describe*",
      "ec2:GetReservedInstancesExchangeQuote",
      "ecs:List*",
      "ecs:Describe*",
      "elasticache:Describe*",
      "elasticache:List*",
      "elasticbeanstalk:Check*",
      "elasticbeanstalk:Describe*",
      "elasticbeanstalk:List*",
      "elasticbeanstalk:RequestEnvironmentInfo",
      "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticfilesystem:Describe*",
      "elasticloadbalancing:Describe*",
      "elasticmapreduce:Describe*",
      "elasticmapreduce:List*",
      "es:List*",
      "es:Describe*",
      "firehose:ListDeliveryStreams",
      "firehose:DescribeDeliveryStream",
      "iam:List*",
      "iam:Get*",
      "iam:GenerateCredentialReport",
      "kinesis:Describe*",
      "kinesis:List*",
      "kms:DescribeKey",
      "kms:GetKeyRotationStatus",
      "kms:ListKeys",
      "lambda:List*",
      "logs:Describe*",
      "organizations:ListAccounts",
      "organizations:ListTagsForResource",
      "redshift:Describe*",
      "route53:Get*",
      "route53:List*",
      "rds:Describe*",
      "rds:ListTagsForResource",
      "s3:GetAccountPublicAccessBlock",
      "s3:GetBucketPublicAccessBlock",
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
      "s3:GetBucketLogging",
      "s3:GetBucketPolicy",
      "s3:GetBucketPolicyStatus",
      "s3:GetBucketTagging",
      "s3:GetBucketVersioning",
      "s3:GetBucketWebsite",
      "s3:List*",
      "sagemaker:Describe*",
      "sagemaker:List*",
      "savingsplans:DescribeSavingsPlans",
      "sdb:GetAttributes",
      "sdb:List*",
      "ses:Get*",
      "ses:List*",
      "sns:Get*",
      "sns:List*",
      "sqs:GetQueueAttributes",
      "sqs:ListQueues",
      "storagegateway:List*",
      "storagegateway:Describe*",
      "workspaces:Describe*"
    ]

    resources = ["*"]
  }
}
