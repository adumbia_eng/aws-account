resource "aws_s3_account_public_access_block" "this" {
  provider = aws.account

  account_id              = aws_organizations_account.account.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets

  depends_on = [null_resource.account_delay]
}

resource "aws_iam_account_password_policy" "this" {
  provider = aws.account

  max_password_age               = var.max_password_age
  minimum_password_length        = var.minimum_password_length
  require_lowercase_characters   = var.require_lowercase_characters
  require_numbers                = var.require_numbers
  require_uppercase_characters   = var.require_uppercase_characters
  require_symbols                = var.require_symbols
  allow_users_to_change_password = var.allow_users_to_change_password
  hard_expiry                    = var.hard_expiry
  password_reuse_prevention      = var.password_reuse_prevention
}

# We must enable ebs encryption by default, ideally this should happen with
# a CMK however there may be exceptions to this where software we are running
# doesn't support this (eg: CloudOS).

# So enable it by default
resource "aws_ebs_encryption_by_default" "this" {
  provider = aws.account

  enabled = true

  depends_on = [null_resource.account_delay]
}

# Then if we have a KMS CMK defined then use that
resource "aws_ebs_default_kms_key" "this" {
  count    = var.kms_cmk_ebs_default == "" ? 0 : 1
  provider = aws.account

  key_arn = var.kms_cmk_ebs_default

  depends_on = [null_resource.account_delay]
}

