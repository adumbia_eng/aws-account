# Create the AWS Organizations account
resource "aws_organizations_account" "account" {
  name                       = var.name
  email                      = var.email_owner
  iam_user_access_to_billing = var.iam_user_access_to_billing
  parent_id                  = var.organizational_unit_id
  role_name                  = var.role_name
  tags                       = var.tags

  lifecycle {
    ignore_changes = [role_name, iam_user_access_to_billing]
  }
}

# Delay for 5 mins when creating the account to let AWS provision it fully
resource "null_resource" "account_delay" {
  provisioner "local-exec" {
    command = "sleep 300"
  }
  triggers = {
    "account_id" = aws_organizations_account.account.id
  }
}

# Set the budget for the AWS Organization account
resource "aws_budgets_budget" "budget" {
  name              = "Overall monthly for ${aws_organizations_account.account.name}"
  budget_type       = "COST"
  limit_amount      = var.budget_limit_amount
  limit_unit        = "USD"
  time_period_start = "1970-01-01_00:00"
  time_unit         = "MONTHLY"

  cost_filters = {
    LinkedAccount = aws_organizations_account.account.id
  }

  notification {
    comparison_operator        = "GREATER_THAN"
    threshold                  = 100
    threshold_type             = "PERCENTAGE"
    notification_type          = "FORECASTED"
    subscriber_email_addresses = var.budget_emails
  }

  notification {
    comparison_operator        = "GREATER_THAN"
    threshold                  = 100
    threshold_type             = "PERCENTAGE"
    notification_type          = "ACTUAL"
    subscriber_email_addresses = var.budget_emails
  }
}
