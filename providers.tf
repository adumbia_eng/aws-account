# Provider for the core_shared account
provider "aws" {
  version = "~> 2.52"
}

# Provider for the provisioned AWS Organizations Account
provider "aws" {
  version = "~> 2.52"
  alias   = "account"
  assume_role {
    role_arn = "arn:aws:iam::${aws_organizations_account.account.id}:role/${var.role_name}"
  }
}

# Provider for the core_network account
provider "aws" {
  version = "~> 2.52"
  alias   = "network"
  assume_role {
    role_arn = "arn:aws:iam::${local.network_account}:role/${var.role_name}"
  }
}

provider "okta" {
  org_name  = "ngisengland"
  base_url  = "okta.com"
  api_token = data.aws_secretsmanager_secret_version.okta_token.secret_string
}
