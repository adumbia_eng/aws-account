variable "budget_limit_amount" {
  type        = string
  description = "The amount of cost or usage being measured for a budget."
}

variable "budget_emails" {
  type        = list(string)
  description = "E-Mail addresses to notify for billing alerts."
}

variable "email_owner" {
  type        = string
  description = "The email address of the owner to assign to the new member account. This email address must not already be associated with another AWS account."
}

variable "iam_credentials_report_recipients" {
  type        = list
  description = "List of email addresses to notify when IAM credentials become stale"
}

variable "iam_user_access_to_billing" {
  type        = string
  default     = "ALLOW"
  description = "If set to ALLOW, the new account enables IAM users to access account billing information if they have the required permissions. If set to DENY, then only the root user of the new account can access account billing information."
}

variable "name" {
  type        = string
  description = "A friendly name for the member account."
}

variable "organizational_unit_id" {
  type        = string
  description = "Parent Organizational Unit ID for the account."
}

variable "region" {
  type        = string
  description = "AWS Region"
}

variable "role_name" {
  type        = string
  description = "The name of an IAM role that Organizations automatically preconfigures in the new member account. This role trusts the master account, allowing users in the master account to assume the role, as permitted by the master account administrator. The role has administrator permissions in the new member account."
}

variable "tags" {
  type        = map
  description = "Key-value mapping of resource tags."
}

variable "logging_account" {
  type        = string
  description = "Account number for the Core Logging account"
  default     = ""
}

variable "network_account" {
  type        = string
  description = "Account number for the Core Network account"
  default     = ""
}

variable "dns_vpc_name" {
  type        = string
  description = "TAG Name of the DNS VPC"
  default     = "dns"
}

variable "vpc_name" {
  type        = string
  description = "The name of the VPC to use as it appears in core_networking"
  default     = ""
}

variable "create_private_hosted_zones" {
  type        = bool
  description = "Create the Private hosted zones"
  default     = false
}

variable "private_hosted_zones" {
  type        = list(string)
  description = "List of Privated hostsed zones domains"
  default     = []
}

variable "route53_resolver_rules" {
  type        = list(string)
  description = "List of Resolver rules to assoicate to account VPC"
  default     = []
}

variable "applications" {
  type        = map(map(string))
  description = "map of applications in the account with the `actions` and `resources` used in IAM roles"
  default     = {}
}

variable "shared_account" {
  type        = string
  description = "Shared account number"
  default     = ""
}

variable "ci_tags" {
  type        = map
  description = "Tags for the CI IAM roles"
  default     = {}
}

variable "ci_baseline" {
  type        = string
  description = "Baseline policy file merged with per app policys"
  default     = ""
}

variable "ci_boundary" {
  type        = string
  description = "Boundary policy file"
  default     = ""
}

variable "ci_boundary_baseline" {
  type        = string
  description = "Boundary Baseline policy file"
  default     = ""
}

variable "block_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should block public ACLs for buckets in this account"
  default     = true
}

variable "block_public_policy" {
  type        = bool
  description = "Whether Amazon S3 should block public bucket policies for buckets in this account"
  default     = true
}

variable "ignore_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should ignore public ACLs for buckets in this account"
  default     = true
}

variable "restrict_public_buckets" {
  type        = bool
  description = "Whether Amazon S3 should restrict public bucket policies for buckets in this account"
  default     = true
}

variable "aws_config_enabled" {
  type        = bool
  description = "Enable AWS Config resources"
  default     = false
}

variable "aws_config_bucket_arn" {
  type        = string
  description = "Variables used for aws-config"
  default     = ""
}

variable "aws_config_bucket_name" {
  type        = string
  description = "Variables used for aws-config"
  default     = ""
}

variable "aws_config_role_name" {
  type        = string
  description = "Variables used for aws-config"
  default     = ""
}

variable "aws_config_rules_aws" {
  type        = map
  description = "Map of aws-config rules to enable"
  default     = {}
}

variable "iam_boundary_variables" {
  type        = map
  description = "A map of variables that should be used to render the IAM Permissions Boundary policy"
  default     = {}
}

#~~~~~~~~~~~~~~
# CloudHealth
#~~~~~~~~~~~~~~
variable "allow_cloudhealth_integrations" {
  type        = bool
  description = "Controls creation of CloudHealth integration resources"
  default     = true
}

variable "cloudhealth_partner_external_id" {
  type        = string
  description = "The external ID for the partner tenant in CloudHealth"
}

variable "cloudhealth_customer_external_id" {
  type        = string
  description = "The external ID for the customer tenant in CloudHealth"
}

#~~~~~~~~~~~
# KMS CMKs
#~~~~~~~~~~~
variable "kms_cmk_decrypt" {
  type        = list
  description = "KMS CMKs ARNs able to be used for decryption"
  default     = []
}

variable "kms_cmk_use" {
  type        = list
  description = "KMS CMKs ARNs able to be used for encryption/decryption"
  default     = []
}

variable "kms_cmk_ebs_default" {
  type        = string
  description = "KMS CMKs ARN able to be used for ebs encryption by default"
  default     = ""
}

#~~~~~~~~~~~~~~~~~~~~~~
# IAM Password Policy
#~~~~~~~~~~~~~~~~~~~~~~
variable "max_password_age" {
  type        = number
  description = "The number of days that an user password is valid"
  default     = 90
}

variable "minimum_password_length" {
  type        = number
  description = "Minimum length to require for user passwords"
  default     = 16
}

variable "require_lowercase_characters" {
  type        = bool
  description = "Whether to require lowercase characters for user passwords"
  default     = true
}

variable "require_numbers" {
  type        = bool
  description = "Whether to require numbers for user passwords"
  default     = true
}

variable "require_uppercase_characters" {
  type        = bool
  description = "Whether to require uppercase characters for user passwords"
  default     = true
}

variable "require_symbols" {
  type        = bool
  description = "Whether to require symbols for user passwords"
  default     = true
}

variable "allow_users_to_change_password" {
  type        = bool
  description = "Whether to allow users to change their own password"
  default     = true
}

variable "hard_expiry" {
  type        = bool
  description = "Whether users are prevented from setting a new password after their password has expired (i.e. require administrator reset)"
  default     = false
}

variable "password_reuse_prevention" {
  type        = number
  description = "The number of previous passwords that users are prevented from reusing"
  default     = 12
}


#~~~~~~~~~~~~~~~~~~~~~~
# Terraform resources
#~~~~~~~~~~~~~~~~~~~~~~
variable "terraform_state_manager_role_arns" {
  type        = list(string)
  description = "A list of ARNs of the roles allowed to write to this state bucket."
  default     = []
}

variable "terraform_state_reader_role_arns" {
  type        = list(string)
  description = "A list of ARNs of the roles allowed to read data from this."
  default     = []
}


#~~~~~~~~~~~~~~~~~~~~~~
# Rotate IAM Credentials
#~~~~~~~~~~~~~~~~~~~~~~

variable "ses_iam_role_arn" {
  type = string
}

variable "ses_email_source" {
  type = string
}

variable "ses_email_source_arn" {
  type = string
}

#~~~~~~~~~~~~~~~~~~~~~~
# Okta SSO
#~~~~~~~~~~~~~~~~~~~~~~

variable "environment" {
  type = string
}

variable "ad_groups" {
  type    = map
  default = {}
}