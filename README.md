# AWS account Terraform module

Provide a standard way of creating an AWS account.

## Components

### Managed in the master account
* Account
* Budget
* SSO Mapping

### Managed in the account itself
* `aws-deployer` IAM user to deploy AWS resources
* Versioned S3 bucket for storing Terraform state
* DynamoDB table to lock Terraform state
* Account level S3 public access block

### Managed in Okta
* AD -> Okta Group
* AD -> Okta Rule
* Okta Group -> Okta AWS Application
## Example
```hcl

locals {
  bio-pipelines_apps = {
    Foo = {
      policy = "${path.module}/policies/CIAppRole_bio-pipelines_Foo.json"
    },
    Bar = {
      policy = "${path.module}/policies/CIAppRole_bio-pipelines_Bar.json"
    }
  }
}

module "account_bio-pipelines_dev" {
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/aws-account?ref=2020.03.12"

  # Account Information
  name        = "dev_bio-pipelines"
  email_owner = "cloud-platform+aws_accounts+bio@genomicsengland.co.uk"

  tags = {
    dept = "cloud-platform",
    func = "AWS organization",
  }
  
  # Organization Information
  organizational_unit_id = aws_organizations_organizational_unit.dev.id
  role_name              = "OrgDeploy"

  environment = var.root
  ad_groups = {
    AWS-SSO-Access-BioPipelines_Admin = ["AdministratorAccess", "Billing"]
  }

  # CICD 
  applications         = local.bio-pipelines_apps
  shared_account       = var.core_shared_account_id
  ci_baseline          = "${path.module}/policies/Baseline.json"
  ci_boundary          = "${path.module}/policies/Boundary.json"
  ci_boundary_baseline = "${path.module}/policies/BoundaryBaseline.json"

  ci_tags = merge(var.default_tags, {
    "Department" : "Bio Informatics",
    "Squad" : "Bio Pipelines"
    }

  # Budgets
  budget_limit_amount = "1000.0"
  budget_emails = [
    "user1@example.com",
    "user2@example.com",
  ]

  # DNS
  create_private_hosted_zones = true
  vpc_name                    = "test"
  network_account             = "112596477968"
  route53_resolver_rules      = ["rslvr-rr-123456789"]
  private_hosted_zones        = ["test.test-aws.gel.ac", "nhs.test.nhs.test"]          
  
}
```

## Terraform Remote State

Is encrypted by default using a KMS CMK.  The module is made aware of the CMK through the `client_account_cmk_arn` variable. 
Write access is granted to this bucket by default to all the application deploy roles defined in the `applications` variable.

Additional roles can be granted read/write (reader/manager) access by adding them to the respective list variables:

```hcl
terraform_state_manager_role_arns
terraform_state_reader_role_arns
```

The `terraform_state_manager_role_arns` has been designed with the migration from the `OrgDeploy` admin role to the `CIDeploy` role in mind.
A suggested implementation for these variables is as follows:

```hcl
module "account-example_dev" {
  # ...

  terraform_state_manager_role_arns = [
    "arn:aws:iam::<account-example_dev_id>:role/OrgDeploy"
  ]

  terraform_state_reader_role_arns = [
    "arn:aws:iam::<core_share_account_id>:role/OrgDeploy", # core_shared
    "arn:aws:iam::<core_security_account_id>:role/CIDeploy" # core_security
  ]

  # ...
}
```

## AWS Config

When enabling AWS Config for an account you will now have a few more inputs and subkeys for the aws managed rules.

| Input | Subkey | Description | Type | Default | Required |
|---|---|---|---|---|---|
| aws_config_rules_aws | `rule_name` |Map of AWS Built in roles to use | `map` | `{}` | no |

### Supported rules

All supported rules use the lowercase name from the [AWS documentation](https://docs.aws.amazon.com/config/latest/developerguide/managed-rules-by-aws-config.html). For example [Restricted SSH](https://docs.aws.amazon.com/config/latest/developerguide/restricted-ssh.html) uses `restricted-ssh` as it's name within this module.

| Name | Extra Arguments | Type | Default | Required
|---|---|---|---|---|
| [approved-amis-by-tag](https://docs.aws.amazon.com/config/latest/developerguide/approved-amis-by-tag.html) | allowed_tags | string | `""` | yes |
| [restricted-ssh](https://docs.aws.amazon.com/config/latest/developerguide/restricted-ssh.html) | | | | |

### Example

Due to limitations with Terraform datastructures if a rule doesn't have any eny arguments we need to pass an empty map.

In this example we have the `approved-amis-by-tag` which has extra arguments and `restricted-ssh` which does not.

```tf
module "account_foo" {
  source "../../modules/aws-account"

  aws_config_enabled     = true
  aws_config_bucket_name = aws_s3_bucket.aws-config_platform.bucket
  aws_config_role_name   = aws_iam_role.aws-config_platform.arn

  aws_config_rules_aws = {
    approved-amis-by-tag = {
      allowed_tags = "foo:bar"
    }
    restricted-ssh = {}
  }
}
```
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.52 ~> 2.52 ~> 2.52 |
| aws.account | ~> 2.52 ~> 2.52 ~> 2.52 |
| aws.network | ~> 2.52 ~> 2.52 ~> 2.52 |
| null | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| allow\_cloudhealth\_integrations | Controls creation of CloudHealth integration resources | `bool` | `true` | no |
| allow\_users\_to\_change\_password | Whether to allow users to change their own password | `bool` | `true` | no |
| applications | map of applications in the account with the `actions` and `resources` used in IAM roles | `map(map(string))` | `{}` | no |
| aws\_config\_bucket\_arn | Variables used for aws-config | `string` | `""` | no |
| aws\_config\_bucket\_name | Variables used for aws-config | `string` | `""` | no |
| aws\_config\_enabled | Enable AWS Config resources | `bool` | `false` | no |
| aws\_config\_role\_name | Variables used for aws-config | `string` | `""` | no |
| aws\_config\_rules\_aws | Map of aws-config rules to enable | `map` | `{}` | no |
| block\_public\_acls | Whether Amazon S3 should block public ACLs for buckets in this account | `bool` | `true` | no |
| block\_public\_policy | Whether Amazon S3 should block public bucket policies for buckets in this account | `bool` | `true` | no |
| budget\_emails | E-Mail addresses to notify for billing alerts. | `list(string)` | n/a | yes |
| budget\_limit\_amount | The amount of cost or usage being measured for a budget. | `string` | n/a | yes |
| ci\_baseline | Baseline policy file merged with per app policys | `string` | `""` | no |
| ci\_boundary | Boundary policy file | `string` | `""` | no |
| ci\_boundary\_baseline | Boundary Baseline policy file | `string` | `""` | no |
| ci\_tags | Tags for the CI IAM roles | `map` | `{}` | no |
| cloudhealth\_customer\_external\_id | The external ID for the customer tenant in CloudHealth | `string` | n/a | yes |
| cloudhealth\_partner\_external\_id | The external ID for the partner tenant in CloudHealth | `string` | n/a | yes |
| create\_private\_hosted\_zones | Create the Private hosted zones | `bool` | `false` | no |
| dns\_vpc\_name | TAG Name of the DNS VPC | `string` | `"dns"` | no |
| email\_owner | The email address of the owner to assign to the new member account. This email address must not already be associated with another AWS account. | `string` | n/a | yes |
| hard\_expiry | Whether users are prevented from setting a new password after their password has expired (i.e. require administrator reset) | `bool` | `false` | no |
| iam\_boundary\_variables | A map of variables that should be used to render the IAM Permissions Boundary policy | `map` | `{}` | no |
| iam\_credentials\_report\_recipients | List of email addresses to notify when IAM credentials become stale | `list` | <pre>[<br>  "address@example.com"<br>]</pre> | no |
| iam\_user\_access\_to\_billing | If set to ALLOW, the new account enables IAM users to access account billing information if they have the required permissions. If set to DENY, then only the root user of the new account can access account billing information. | `string` | `"ALLOW"` | no |
| ignore\_public\_acls | Whether Amazon S3 should ignore public ACLs for buckets in this account | `bool` | `true` | no |
| kms\_cmk\_decrypt | KMS CMKs ARNs able to be used for decryption | `list` | `[]` | no |
| kms\_cmk\_ebs\_default | KMS CMKs ARN able to be used for ebs encryption by default | `string` | `""` | no |
| kms\_cmk\_use | KMS CMKs ARNs able to be used for encryption/decryption | `list` | `[]` | no |
| logging\_account | Account number for the Core Logging account | `string` | `""` | no |
| max\_password\_age | The number of days that an user password is valid | `number` | `90` | no |
| minimum\_password\_length | Minimum length to require for user passwords | `number` | `16` | no |
| name | A friendly name for the member account. | `string` | n/a | yes |
| network\_account | Account number for the Core Network account | `string` | `""` | no |
| organizational\_unit\_id | Parent Organizational Unit ID for the account. | `string` | n/a | yes |
| password\_reuse\_prevention | The number of previous passwords that users are prevented from reusing | `number` | `12` | no |
| private\_hosted\_zones | List of Privated hostsed zones domains | `list(string)` | `[]` | no |
| region | AWS Region | `string` | n/a | yes |
| require\_lowercase\_characters | Whether to require lowercase characters for user passwords | `bool` | `true` | no |
| require\_numbers | Whether to require numbers for user passwords | `bool` | `true` | no |
| require\_symbols | Whether to require symbols for user passwords | `bool` | `true` | no |
| require\_uppercase\_characters | Whether to require uppercase characters for user passwords | `bool` | `true` | no |
| restrict\_public\_buckets | Whether Amazon S3 should restrict public bucket policies for buckets in this account | `bool` | `true` | no |
| role\_name | The name of an IAM role that Organizations automatically preconfigures in the new member account. This role trusts the master account, allowing users in the master account to assume the role, as permitted by the master account administrator. The role has administrator permissions in the new member account. | `string` | n/a | yes |
| route53\_resolver\_rules | List of Resolver rules to assoicate to account VPC | `list(string)` | `[]` | no |
| ses\_email\_source | n/a | `string` | n/a | yes |
| ses\_email\_source\_arn | n/a | `string` | n/a | yes |
| ses\_iam\_role\_arn | n/a | `string` | n/a | yes |
| shared\_account | Shared account number | `string` | `""` | no |
| tags | Key-value mapping of resource tags. | `map` | n/a | yes |
| terraform\_state\_manager\_role\_arns | A list of ARNs of the roles allowed to write to this state bucket. | `list(string)` | `[]` | no |
| terraform\_state\_reader\_role\_arns | A list of ARNs of the roles allowed to read data from this. | `list(string)` | `[]` | no |
| vpc\_name | The name of the VPC to use as it appears in core\_networking | `string` | `""` | no |
| environment | The organisation environment being deployed to | `string` | n/a | yes |
| okta_id | The Access Token for accessing Okta | `string` | n/a | yes |
| ad_groups | Map of Active Directory group and their associated list of PermissionSets which need to be mapped in to AWS SSO via Okta (Groups & PermissionSets_must_ exist in AD/Okta/AWS SSO) | `list(string)` | `[]` | no |
## Outputs

| Name | Description |
|------|-------------|
| account\_arn | The ARN for this account |
| account\_id | The AWS account id |
| cloudhealth\_role\_arn | ARN of the CloudHealth IAM role |
| terraform\_bucket\_arn | ARN of the Terraform State bucket |
| terraform\_bucket\_id | ID of the Terraform State bucket |

