## Platform Config Rules
##
## Due to limitations with Terraform datastructures this isn't easily
## templatable so we need to define each rule with it's options then check if
## the rule is enabled for an account or not.
##
## To check if we should be managing the rule we check if the name of the rule
## is contained in `var.aws_config_rules_aws`.
##
## If there are any other arguments we need for the rule then we can lookup
## subkeys of var.aws_config_rules_aws["name"], for an example see
## `aws_config_config_rule.approved-amis-by-tag`.

# approved-amis-by-tag
# https://docs.aws.amazon.com/config/latest/developerguide/approved-amis-by-tag.html

locals {
  component_tag = { "Component" : "Config Rule" }
}

resource "aws_config_config_rule" "approved-amis-by-tag" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "approved-amis-by-tag"
  ) ? 1 : 0

  name        = "platform_approved-amis-by-tag"
  description = "Checks whether running instances are using specified AMIs. Specify the tags that identify the AMIs. Running instances with AMIs that don't have at least one of the specified tags are NON_COMPLIANT."

  input_parameters = jsonencode({
    "amisByTagKeyAndValue" = var.aws_config_rules_aws["approved-amis-by-tag"]["allowed_tags"]
  })

  source {
    owner             = "AWS"
    source_identifier = "APPROVED_AMIS_BY_TAG"
  }

  scope {
    compliance_resource_types = ["AWS::EC2::Instance"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# restricted-ssh
# https://docs.aws.amazon.com/config/latest/developerguide/restricted-ssh.html
resource "aws_config_config_rule" "restricted-ssh" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "restricted-ssh"
  ) ? 1 : 0

  name        = "platform_restricted-ssh"
  description = "Checks whether the incoming SSH traffic for the security groups is accessible. The rule is COMPLIANT when IP addresses of the incoming SSH traffic in the security groups are restricted. This rule applies only to IPv4."

  source {
    owner             = "AWS"
    source_identifier = "INCOMING_SSH_DISABLED"
  }

  scope {
    compliance_resource_types = ["AWS::EC2::SecurityGroup"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# ec2-instance-no-public-ip
# https://asecure.cloud/a/cfgrule_ec2-instance-no-public-ip/
resource "aws_config_config_rule" "ec2-instance-no-public-ip" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "ec2-instance-no-public-ip"
  ) ? 1 : 0

  name        = "ec2-instance-no-public-ip"
  description = "Checks whether Amazon Elastic Compute Cloud (Amazon EC2) instances have a public IP association. The rule is NON_COMPLIANT if the publicIp field is present in the Amazon EC2 instance configuration item. This rule applies only to IPv4"

  source {
    owner             = "AWS"
    source_identifier = "EC2_INSTANCE_NO_PUBLIC_IP"
  }

  scope {
    compliance_resource_types = ["AWS::EC2::Instance"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# ec2-security-group-attached-to-eni
# https://asecure.cloud/a/cfgrule_ec2-security-group-attached-to-eni/
resource "aws_config_config_rule" "ec2-security-group-attached-to-eni" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "ec2-security-group-attached-to-eni"
  ) ? 1 : 0

  name        = "ec2-security-group-attached-to-eni"
  description = "Checks that security groups are attached to Amazon Elastic Compute Cloud (Amazon EC2) instances or an elastic network interfaces (ENIs). The rule returns NON_COMPLIANT if the security group is not associated with an Amazon EC2 instan..."

  source {
    owner             = "AWS"
    source_identifier = "EC2_SECURITY_GROUP_ATTACHED_TO_ENI"
  }
  scope {
    compliance_resource_types = ["AWS::EC2::SecurityGroup"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# internet-gateway-authorized-vpc-only
# https://asecure.cloud/a/cfgrule_internet-gateway-authorized-vpc-only/
resource "aws_config_config_rule" "internet-gateway-authorized-vpc-only" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "internet-gateway-authorized-vpc-only"
  ) ? 1 : 0

  name        = "internet-gateway-authorized-vpc-only"
  description = "Checks that Internet gateways (IGWs) are only attached to an authorized Amazon Virtual Private Cloud (VPCs). The rule is NON_COMPLIANT if IGWs are not attached to an authorized VPC."

  source {
    owner             = "AWS"
    source_identifier = "INTERNET_GATEWAY_AUTHORIZED_VPC_ONLY"
  }
  scope {
    compliance_resource_types = ["AWS::EC2::InternetGateway"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# vpc-default-security-group-closed
# https://asecure.cloud/a/cfgrule_vpc-default-security-group-closed/
resource "aws_config_config_rule" "vpc-default-security-group-closed" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "vpc-default-security-group-closed"
  ) ? 1 : 0

  name        = "vpc-default-security-group-closed"
  description = "Checks that the default security group of any Amazon Virtual Private Cloud (VPC) does not allow inbound or outbound traffic. The rule returns NOT_APPLICABLE if the security group is not default. The rule is NON_COMPLIANT if the defau..."

  source {
    owner             = "AWS"
    source_identifier = "VPC_DEFAULT_SECURITY_GROUP_CLOSED"
  }
  scope {
    compliance_resource_types = ["AWS::EC2::SecurityGroup"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# vpc-flow-logs-enabled
# https://asecure.cloud/a/cfgrule_vpc-flow-logs-enabled/
resource "aws_config_config_rule" "vpc-flow-logs-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "vpc-flow-logs-enabled"
  ) ? 1 : 0

  name        = "vpc-flow-logs-enabled"
  description = "Checks whether Amazon Virtual Private Cloud flow logs are found and enabled for Amazon VPC."

  source {
    owner             = "AWS"
    source_identifier = "VPC_FLOW_LOGS_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# encrypted-volumes
# https://asecure.cloud/a/encrypted-volumes/
resource "aws_config_config_rule" "encrypted-volumes" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "encrypted-volumes"
  ) ? 1 : 0

  name        = "encrypted-volumes"
  description = "Checks whether the EBS volumes that are in an attached state are encrypted. If you specify the ID of a KMS key for encryption using the kmsId parameter, the rule checks if the EBS volumes in an attached state are encrypted with that ..."

  source {
    owner             = "AWS"
    source_identifier = "ENCRYPTED_VOLUMES"
  }
  scope {
    compliance_resource_types = ["AWS::EC2::Volume"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# cloudtrail-s3-dataevents-enabled
# https://asecure.cloud/a/cfgrule_cloudtrail-s3-dataevents-enabled/
resource "aws_config_config_rule" "cloudtrail-s3-dataevents-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "cloudtrail-s3-dataevents-enabled"
  ) ? 1 : 0

  name        = "cloudtrail-s3-dataevents-enabled"
  description = "Checks whether at least one AWS CloudTrail trail is logging Amazon S3 data events for all S3 buckets. The rule is NON_COMPLIANT if trails that log data events for S3 buckets are not configured."

  source {
    owner             = "AWS"
    source_identifier = "CLOUDTRAIL_S3_DATAEVENTS_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-account-level-public-access-blocks
# https://asecure.cloud/a/cfgrule_s3-account-level-public-access-blocks/
resource "aws_config_config_rule" "s3-account-level-public-access-blocks" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-account-level-public-access-blocks"
  ) ? 1 : 0

  name        = "s3-account-level-public-access-blocks"
  description = "Checks whether the required public access block settings are configured from account level. The rule is only NON_COMPLIANT when the fields set below do not match the corresponding fields in the configuration item."

  input_parameters = "{\"IgnorePublicAcls\":\"True\",\"BlockPublicPolicy\":\"True\",\"BlockPublicAcls\":\"True\",\"RestrictPublicBuckets\":\"True\"}"

  source {
    owner             = "AWS"
    source_identifier = "S3_ACCOUNT_LEVEL_PUBLIC_ACCESS_BLOCKS"
  }
  scope {
    compliance_resource_types = ["AWS::S3::AccountPublicAccessBlock"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-bucket-logging-enabled
# https://asecure.cloud/a/s3-bucket-logging-enabled/
resource "aws_config_config_rule" "s3-bucket-logging-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-bucket-logging-enabled"
  ) ? 1 : 0

  name        = "s3-bucket-logging-enabled"
  description = "Checks whether logging is enabled for your S3 buckets."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_LOGGING_ENABLED"
  }
  scope {
    compliance_resource_types = ["AWS::S3::Bucket"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-bucket-public-read-prohibited
# https://asecure.cloud/a/s3-bucket-public-read-prohibited/
resource "aws_config_config_rule" "s3-bucket-public-read-prohibited" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-bucket-public-read-prohibited"
  ) ? 1 : 0

  name        = "s3-bucket-public-read-prohibited"
  description = "Checks that your Amazon S3 buckets do not allow public read access. If an Amazon S3 bucket policy or bucket ACL allows public read access, the bucket is noncompliant."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_PUBLIC_READ_PROHIBITED"
  }
  scope {
    compliance_resource_types = ["AWS::S3::Bucket"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-bucket-public-write-prohibited
# https://asecure.cloud/a/s3-bucket-public-write-prohibited/
resource "aws_config_config_rule" "s3-bucket-public-write-prohibited" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-bucket-public-write-prohibited"
  ) ? 1 : 0

  name        = "s3-bucket-public-write-prohibited"
  description = "Checks that your Amazon S3 buckets do not allow public write access. If an Amazon S3 bucket policy or bucket ACL allows public write access, the bucket is noncompliant."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_PUBLIC_WRITE_PROHIBITED"
  }
  scope {
    compliance_resource_types = ["AWS::S3::Bucket"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-bucket-server-side-encryption-enabled
# https://asecure.cloud/a/s3-bucket-server-side-encryption-enabled/
resource "aws_config_config_rule" "s3-bucket-server-side-encryption-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-bucket-server-side-encryption-enabled"
  ) ? 1 : 0

  name        = "s3-bucket-server-side-encryption-enabled"
  description = "Checks that your Amazon S3 bucket either has Amazon S3 default encryption enabled or that the S3 bucket policy explicitly denies put-object requests without server side encryption."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED"
  }
  scope {
    compliance_resource_types = ["AWS::S3::Bucket"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# s3-bucket-ssl-requests-only
# https://asecure.cloud/a/s3-bucket-ssl-requests-only/
resource "aws_config_config_rule" "s3-bucket-ssl-requests-only" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "s3-bucket-ssl-requests-only"
  ) ? 1 : 0

  name        = "s3-bucket-ssl-requests-only"
  description = "Checks whether S3 buckets have policies that require requests to use Secure Socket Layer (SSL)."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_SSL_REQUESTS_ONLY"
  }
  scope {
    compliance_resource_types = ["AWS::S3::Bucket"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# access-keys-rotated
# https://asecure.cloud/a/cfgrule_access-keys-rotated/
# Input maxAccessKeyAge is set to 90 days - amend as required
resource "aws_config_config_rule" "access-keys-rotated" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "access-keys-rotated"
  ) ? 1 : 0

  name        = "access-keys-rotated"
  description = "Checks whether the active access keys are rotated within the number of days specified in maxAccessKeyAge. The rule is NON_COMPLIANT if the access keys have not been rotated for more than maxAccessKeyAge number of days."

  input_parameters = jsonencode({
    "maxAccessKeyAge" = var.aws_config_rules_aws["access-keys-rotated"]["access_key_age"]
  })

  source {
    owner             = "AWS"
    source_identifier = "ACCESS_KEYS_ROTATED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-password-policy
# https://asecure.cloud/a/iam-password-policy/
resource "aws_config_config_rule" "iam-password-policy" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-password-policy"
  ) ? 1 : 0

  name        = "iam-password-policy"
  description = "Checks whether the account password policy for IAM users meets the specified requirements."

  input_parameters = jsonencode({
    "RequireUppercaseCharacters" = var.aws_config_rules_aws["iam-password-policy"]["require_upper_case"],
    "RequireLowercaseCharacters" = var.aws_config_rules_aws["iam-password-policy"]["require_lower_case"],
    "RequireSymbols"             = var.aws_config_rules_aws["iam-password-policy"]["require_symbols"],
    "RequireNumbers"             = var.aws_config_rules_aws["iam-password-policy"]["require_numbers"],
    "MinimumPasswordLength"      = var.aws_config_rules_aws["iam-password-policy"]["password_length"],
    "PasswordReusePrevention"    = var.aws_config_rules_aws["iam-password-policy"]["password_reuse"],
    "MaxPasswordAge"             = var.aws_config_rules_aws["iam-password-policy"]["password_age"]
  })

  source {
    owner             = "AWS"
    source_identifier = "IAM_PASSWORD_POLICY"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-policy-no-statements-with-admin-access
# https://asecure.cloud/a/cfgrule_iam-policy-no-statements-with-admin-access/
resource "aws_config_config_rule" "iam-policy-no-statements-with-admin-access" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-policy-no-statements-with-admin-access"
  ) ? 1 : 0

  name        = "iam-policy-no-statements-with-admin-access"
  description = "Checks whether the default version of AWS Identity and Access Management (IAM) policies do not have administrator access. If any statement has 'Effect': 'Allow' with 'Action': '*' over 'Resource': '*', the rule is NON_COMPLIANT."

  source {
    owner             = "AWS"
    source_identifier = "IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS"
  }
  scope {
    compliance_resource_types = ["AWS::IAM::Policy"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-root-access-key-check
# https://asecure.cloud/a/cfgrule_iam-root-access-key-check/
resource "aws_config_config_rule" "iam-root-access-key-check" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-root-access-key-check"
  ) ? 1 : 0

  name        = "iam-root-access-key-check"
  description = "Checks whether the root user access key is available. The rule is COMPLIANT if the user access key does not exist."

  source {
    owner             = "AWS"
    source_identifier = "IAM_ROOT_ACCESS_KEY_CHECK"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-user-mfa-enabled
# https://asecure.cloud/a/cfgrule_iam-user-mfa-enabled/
resource "aws_config_config_rule" "iam-user-mfa-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-user-mfa-enabled"
  ) ? 1 : 0

  name        = "iam-user-mfa-enabled"
  description = "Checks whether the AWS Identity and Access Management users have multi-factor authentication (MFA) enabled."

  source {
    owner             = "AWS"
    source_identifier = "IAM_USER_MFA_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-user-no-policies-check
# https://asecure.cloud/a/iam-user-no-policies-check/
resource "aws_config_config_rule" "iam-user-no-policies-check" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-user-no-policies-check"
  ) ? 1 : 0

  name        = "iam-user-no-policies-check"
  description = "Checks that none of your IAM users have policies attached. IAM users must inherit permissions from IAM groups or roles."

  source {
    owner             = "AWS"
    source_identifier = "IAM_USER_NO_POLICIES_CHECK"
  }
  scope {
    compliance_resource_types = ["AWS::IAM::User"]
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# iam-user-unused-credentials-check
# https://asecure.cloud/a/cfgrule_iam-user-unused-credentials-check/
# Input maxCredentialUsageAge is set to 90 days - amend as required
resource "aws_config_config_rule" "iam-user-unused-credentials-check" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "iam-user-unused-credentials-check"
  ) ? 1 : 0

  name        = "iam-user-unused-credentials-check"
  description = "Checks whether your AWS Identity and Access Management (IAM) users have passwords or active access keys that have not been used within the specified number of days you provided. Re-evaluating this rule within 4 hours of the first eva..."

  input_parameters = jsonencode({
    "maxCredentialUsageAge" = var.aws_config_rules_aws["iam-user-unused-credentials-check"]["unused_credentials_check"]
  })

  source {
    owner             = "AWS"
    source_identifier = "IAM_USER_UNUSED_CREDENTIALS_CHECK"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# mfa-enabled-for-iam-console-access
# https://asecure.cloud/a/cfgrule_mfa-enabled-for-iam-console-access/
resource "aws_config_config_rule" "mfa-enabled-for-iam-console-access" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "mfa-enabled-for-iam-console-access"
  ) ? 1 : 0

  name        = "mfa-enabled-for-iam-console-access"
  description = "Checks whether AWS Multi-Factor Authentication (MFA) is enabled for all AWS Identity and Access Management (IAM) users that use a console password. The rule is COMPLIANT if MFA is enabled."

  source {
    owner             = "AWS"
    source_identifier = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# root-account-hardware-mfa-enabled
# https://asecure.cloud/a/cfgrule_root-account-hardware-mfa-enabled/
resource "aws_config_config_rule" "root-account-hardware-mfa-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "root-account-hardware-mfa-enabled"
  ) ? 1 : 0

  name        = "root-account-hardware-mfa-enabled"
  description = "Checks whether your AWS account is enabled to use multi-factor authentication (MFA) hardware device to sign in with root credentials. The rule is NON_COMPLIANT if any virtual MFA devices are permitted for signing in with root credent..."

  source {
    owner             = "AWS"
    source_identifier = "ROOT_ACCOUNT_HARDWARE_MFA_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# root-account-mfa-enabled
# https://asecure.cloud/a/root-account-mfa-enabled/
resource "aws_config_config_rule" "root-account-mfa-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "root-account-mfa-enabled"
  ) ? 1 : 0

  name        = "root-account-mfa-enabled"
  description = "Checks whether users of your AWS account require a multi-factor authentication (MFA) device to sign in with root credentials."

  source {
    owner             = "AWS"
    source_identifier = "ROOT_ACCOUNT_MFA_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# cmk-backing-key-rotation-enabled
# https://asecure.cloud/a/cfgrule_cmk-backing-key-rotation-enabled/
resource "aws_config_config_rule" "cmk-backing-key-rotation-enabled" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "cmk-backing-key-rotation-enabled"
  ) ? 1 : 0

  name        = "cmk-backing-key-rotation-enabled"
  description = "Checks that key rotation is enabled for each customer master key (CMK). The rule is COMPLIANT, if the key rotation is enabled for specific key object. The rule is not applicable to CMKs that have imported key material."

  source {
    owner             = "AWS"
    source_identifier = "CMK_BACKING_KEY_ROTATION_ENABLED"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}

# kms-cmk-not-scheduled-for-deletion
# https://asecure.cloud/a/cfgrule_kms-cmk-not-scheduled-for-deletion/
resource "aws_config_config_rule" "kms-cmk-not-scheduled-for-deletion" {
  provider = aws.account
  count = contains(
    keys(var.aws_config_rules_aws), "kms-cmk-not-scheduled-for-deletion"
  ) ? 1 : 0

  name        = "kms-cmk-not-scheduled-for-deletion"
  description = "Checks whether customer master keys (CMKs) are not scheduled for deletion in AWS Key Management Service (KMS). The rule is NON_COMPLIANT if CMKs are scheduled for deletion."

  source {
    owner             = "AWS"
    source_identifier = "KMS_CMK_NOT_SCHEDULED_FOR_DELETION"
  }
  scope {
    compliance_resource_types = []
  }

  depends_on = [
    aws_config_configuration_recorder.platform
  ]

  tags = merge(
    var.tags,
    local.config_tags,
    local.component_tag
  )
}