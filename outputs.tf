output "account_arn" {
  value       = aws_organizations_account.account.arn
  description = "The ARN for this account"
}

output "account_id" {
  value       = aws_organizations_account.account.id
  description = "The AWS account id"
}

output "terraform_bucket_id" {
  value       = aws_s3_bucket.terraform.id
  description = "ID of the Terraform State bucket"
}

output "terraform_bucket_arn" {
  value       = aws_s3_bucket.terraform.arn
  description = "ARN of the Terraform State bucket"
}

output "cloudhealth_role_arn" {
  value       = concat(aws_iam_role.cloudhealth.*.arn, [""])[0]
  description = "ARN of the CloudHealth IAM role"
}