# Setup some local's we need for provisioning DNS
locals {
  dns_service_tag      = { "Service" : "Route53 DNS Resolver" }
  private_hosted_zones = var.create_private_hosted_zones ? var.private_hosted_zones : []
  network_account      = var.create_private_hosted_zones || length(var.route53_resolver_rules) > 0 ? var.network_account : aws_organizations_account.account.id
}

# Data for the DNS VPC in the Network Account
data "aws_vpc" "dns_vpc" {
  count = var.create_private_hosted_zones ? 1 : 0

  provider = aws.network
  default  = false
  filter {
    name   = "tag:Name"
    values = [var.dns_vpc_name]
  }
}

# Data for the Environment VPC in the Network Account
data "aws_vpc" "local_vpc" {
  count = var.create_private_hosted_zones || length(var.route53_resolver_rules) > 0 ? 1 : 0

  provider = aws.network
  default  = false
  filter {
    name   = "tag:Name"
    values = [var.vpc_name]
  }
}

# Create Private Hosted Zone in the AWS Organization Account
resource "aws_route53_zone" "private" {
  for_each = toset(local.private_hosted_zones)

  provider = aws.account

  name = each.value
  vpc {
    vpc_id = data.aws_vpc.local_vpc[0].id
  }
  tags = merge(var.tags, local.dns_service_tag, {
    "Name" : each.value
  })

  lifecycle {
    ignore_changes = [vpc]
  }

  depends_on = [null_resource.account_delay]

}

# Create Route53 Association Auth
# This has to shell out to `awscli` as this is not available in Terraform
resource "null_resource" "create_association_authorization" {
  for_each = toset(local.private_hosted_zones)

  triggers = {
    zone_id = aws_route53_zone.private[each.key].zone_id
  }
  provisioner "local-exec" {
    command = "${path.module}/scripts/private_dns.sh"

    environment = {
      PHZ          = aws_route53_zone.private[each.key].zone_id
      DNSVPC       = data.aws_vpc.dns_vpc[0].id
      NETACCID     = local.network_account
      LOCACCID     = aws_organizations_account.account.id
      ROLE         = var.role_name
      ROLESESSNAME = "phz_vpc_association_authorization"
    }
  }

  depends_on = [null_resource.account_delay]
}

# Route53 Rile Association
resource "aws_route53_resolver_rule_association" "rule_association" {
  for_each = toset(var.route53_resolver_rules)

  provider = aws.account

  resolver_rule_id = each.value
  vpc_id           = data.aws_vpc.local_vpc[0].id

  depends_on = [null_resource.account_delay]
}
